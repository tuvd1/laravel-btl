<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    use HasFactory;
    protected $table ='categorys';
    protected $primaryKey = 'id';
    protected $fillable = [
        'category_name','title','image_name','featured','active'
    ];

    public function foods()
    {
        return $this->hasMany(FoodModel::class, 'category_id', 'id');
    }
}

