<?php

namespace App\Http\Controllers;

use App\Models\CategoryModel;
use App\Models\FoodModel;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foods= FoodModel::all();
        return view('BackEnd.foods.food')->with('foods',$foods);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorys = CategoryModel::all();
        return view('BackEnd.foods.create',compact('categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categorys = CategoryModel::find($request->category_id);
        if($request->has('file_upload'))
        {
            $file = $request->file_upload;
            $ext = $request -> file_upload ->extension();
            $file_name = time().'-'.'foods.'.$ext;
            $file->move(public_path('uploads/images/foods'), $file_name);
        }
        $request->merge(['image_name' => $file_name]);
        // FoodModel::create($request->all());
        $categorys->foods()->create([
            'title'=> $request->title,
            'image_name'=> $request->image_name,
            'price'=> $request->price,
            'featured'=> $request->featured,
            'desciption'=> $request->desciption,
            'active'=> $request->active,
        ]);
        return redirect(Route('show_food'))->with('flash_message', 'Food Added!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $categorys = CategoryModel::all();
        $foods = FoodModel::find($id);
        return view ('BackEnd.foods.edit',compact('categorys','foods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->has('file_upload'))
        {
            $file = $request->file_upload;
            $ext = $request -> file_upload ->extension();
            $file_name = time().'-'.'foods.'.$ext;
            $file->move(public_path('uploads/images/foods'), $file_name);
        }
        $request->merge(['image_name' => $file_name]);

        $uses = FoodModel::find($id);
        $input = $request->all();
        $uses -> update($input);
        return redirect(Route('show_food'))->with('flash_message', 'Food Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FoodModel::destroy($id);
        return redirect(Route('show_food'))->with('flash_message', 'Food deleted!');
    }
}
