<?php

namespace App\Http\Controllers;

use App\Models\OrderModel;
use App\Models\FoodModel;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $orders;
    public function __construct(FoodModel $foods, OrderModel $orders)
    {
        $this->orders = $orders;
    }
    public function index()
    {
        // $orders=$this->orders->with('foods')->get();
        return view('Frontend.orders.order');
    }
    public function store($id, Request $request)
    {
        $qty = 0;
        $orders = $this->orders->where('food_id', $id)->first();
        if ($orders != null) {
            $qty =  $orders-> qty +  $request->qty;
            $this->orders->updateOrCreate(
                ['food_id' => $id],
                [
                    'qty' => $qty,
                ]
            );
        } else {
            $this->orders->updateOrCreate(
                ['id' => $request->id ?? null],
                [
                    'food_id' => $id,
                    'food_price' => $request->food_price,
                    'food_title' => $request->food_title,
                    'food_qty' => $request->food_qty,
                    'food_status' => $request->food_status,
                ]
            );
        }
        return redirect()->back();
    }
    public function destroy($id){
        $this->orders->find($id)->delete();
        return redirect()->route('orders.order');
    }
}
