<?php

namespace App\Http\Controllers;

use App\Models\CategoryModel;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorys= CategoryModel::all();
        return view('BackEnd.categories.category')->with('categorys',$categorys);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('BackEnd.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'image_name' => 'required|mimes:jgp,png,jpeg|max:5048'
        // ]);
        
        if($request->has('file_upload'))
        {
            $file = $request->file_upload;
            $ext = $request -> file_upload ->extension();
            $file_name = time().'-'.'categorys.'.$ext;
            $file->move(public_path('uploads/images/categories'), $file_name);
        }
        $request->merge(['image_name' => $file_name]);
        CategoryModel::create($request->all());
        return redirect(Route('show_category'))->with('flash_message', 'Admin Added!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorys = CategoryModel::find($id);
        return view ('BackEnd.categories.edit')->with('categorys',$categorys);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->has('file_upload'))
        {
            $file = $request->file_upload;
            $ext = $request -> file_upload ->extension();
            $file_name = time().'-'.'foods.'.$ext;
            $file->move(public_path('uploads/images/categories'), $file_name);
        }
        $request->merge(['image_name' => $file_name]);

        $uses = CategoryModel::find($id);
        $input = $request->all();
        $uses -> update($input);
        return redirect(Route('show_category'))->with('flash_message', 'Admin Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CategoryModel::destroy($id);
        return redirect(Route('show_category'))->with('flash_message', 'Admin deleted!');
    }
}
