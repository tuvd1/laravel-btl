@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Food</h1>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="offset-3 col-md-5 my-lg-5">   
                <div class="card">
                    <div class="card-header text-center">
                        Edit Food
                    </div>
                    <div class="card-body">
                        <form action="{{Route('update_food',['id'=>$foods->id])}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="">Title</label>
                                <input type="text" class="form-control" name="title" id="title" placeholder="--Title" value="{{$foods-> title}}">
                            </div>
                            <div class="form-group">
                                <label for="">Image_name</label>
                                <input type="file" class="form-control" name="file_upload" id="image_name" placeholder="--Image_name" value="{{$foods-> image_name}}">
                            </div>
                            <div class="form-group">
                                <label for="">Price</label>
                                <input type="number" class="form-control" name="price" id="price" placeholder="--Price" value="{{$foods-> price}}">
                            </div>
                            <div class="form-group">
                                <label for="">Featured</label>
                                <input type="text" class="form-control" name="featured" id="featured" placeholder="--Featured" value="{{$foods-> featured}}">
                            </div>
                            <div class="form-group">
                                <label for="">Desciption</label>
                                <input type="text" class="form-control" name="desciption" id="desciption" placeholder="--Desciption" value="{{$foods-> desciption}}">
                            </div>
                            <div class="form-group">
                                <label for="">Select Category</label>
                                <select name="category_id" class="form-control" id="">
                                    @foreach ($categorys as $item )
                                        <option value="{{$item ->id}}" {{$foods->category_id == $item->id ? 'seleted':''}}>{{$item-> category_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Active</label>
                                <div class="radio">
                                    <input type="radio" name="active" value="Active">Active
                                    <input type="radio" name="active" value="Inactive">Inactive
                                </div>
                            </div>
                            <button type="submit" value="Save" name="btn" class="btn btn-outline-primary btn-block">Food Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop