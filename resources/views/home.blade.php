@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
<a href="{{Route('show_category')}}" class="btn btn-success btn-sm" title="Category">
    Category
</a>
<a href="{{Route('show_food')}}" class="btn btn-success btn-sm" title="Food">
    Food
</a>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
