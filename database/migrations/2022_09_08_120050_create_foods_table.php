<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foods', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id');
            $table->string('title',500)->nullable();
            $table->string('desciption',500)->nullable();
            $table->string('price')->nullable();
            $table->string('image_name',500)->nullable();
            $table->string('featured',500)->nullable();
            $table->string('active',500)->nullable();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categorys')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foods');
    }
};
